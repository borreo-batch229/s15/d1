// js comments ctr /
	// multi-line comments ctr shf /

// STATEMENTS
// programming instruction that we tell a computer to perform
// statements end with ;

//SYNTAX
// it is the set of rules that describes how statements must be constructed


// alert("Hello Again");

// This is a statement with a correct syntax
console.log("Hello World!");

// js is a loose type programming language
console. log (   "       Hello World!"       );

//and also with this 
console.
log
(
"Hello Again"
)

// VARIABLES
// it is used as a container or storage

// Declaring variables - tells our device that a variable name is created and is ready to store data.
// Syntax - let/const variableName;

// "let" is a keyword that is usually used to declare a variable.
// OOP object oriented programming - giving identity

let myVariable;
let hello;

console.log(myVariable); 
console.log(hello);

// "=" initialization, giving value

// guidelines
// let variableName = "value"
// const for constant
let firstName = "Michael";

// declaring
let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

// re-assigning
productPrice = 25000;
console.log(productPrice);

let friend = "kate";
friend = "jane";
console.log(friend);

let supplier;
supplier = "John Smith Tradings";
console.log(supplier);

supplier = "Zuitt Store";
console.log(supplier);

// let vs const
// let variable values can be changed, const cannot

// local = enclosed
// global = not enclosed

let outerVariable = "hi";
{
	let innerVariable = "hello again";
	console.log(innerVariable);
	console.log(outerVariable);
}

console.log(outerVariable);
// console.log(innerVariable); <-error

// MULTIPLE VARIABLE DECLARATION
// can be declared in one line

//let productCode = "DC017"
//let productBrand = "Dell"

let productCode = "DC017", productBrand = "Dell";
console.log(productCode,productBrand);

/*Data Types
strings - words
int- numbers
concatenating string = combining string values with + sign */

let country = "Philippines";
let province = "Metro Manila";

let fullAddress = province + ", " + country;
console.log(fullAddress);

// escape character (\)
// "\n" new line

let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

let message = "John's employees went home early";
console.log(message);
message = 'John\'s employees went home early';
console.log(message);

// combining text and string
let grade=98.7;
console.log("John's grade last quarter is " + grade);

// Boolean	- we have 2 boolean values, true and false

let isMarried = false;
let isGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);

// Arrays - store multiple values with the same data types
// type exclusive
// syntax
// let/const arrayName = [elementA, elementB, .....]

let grades = [98.7, 92.1, 90.2];
console.log(grades);

let details = ["John", "Smith", 32, true];
console.log(details);

//Objects
// Hold properties that describes the variable
// Syntax
// let/const objectName = {propertyA: value, propertB: value}

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	contact: ["0123456789","9876543210"],
	address: { 
		housenumber: "345",
		city: "Manila"
	}
}

console.log(person);

let myGrades = {
	firstGrading: 98.7,
	secondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6
}

console.log(myGrades);

// checking data type
console.log(typeof grades);
console.log(typeof person);
console.log(typeof isMarried);

const anime = ["one piece","one punch man","attack on titan"];
// anime = ["kimetsu no yaiba"];
anime[0]="kimetsu no yaiba";
console.log(anime);

// Null vs Undefined
// null = 0 or empty value
// undefined variable has no value upon declaration
let spouse = null; //0,empty
let fullName //undefined